
// EDM include(s):
#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"

#include "TrigT1Result/RoIBResult.h"
#include "TrigT1Result/L1TopoResult.h"

#include "TrigSteering/Lvl1ResultAccessTool.h"

#include "L1TopoRDO/BlockTypes.h"
#include "L1TopoRDO/Helpers.h"
#include "L1TopoRDO/L1TopoTOB.h"

// Local include(s):
#include "RawFilterAlg.h"

RawFilterAlg::RawFilterAlg( const std::string& name, ISvcLocator* svcLoc )
    : AthFilterAlgorithm( name, svcLoc ),
      m_lvl1Tool("HLT::Lvl1ResultAccessTool/Lvl1ResultAccessTool",this),
      m_processed_events(0)
{
    // Set the description of the implemented filter:
    setFilterDescription( "Events with even numbers" );
}

StatusCode RawFilterAlg::initialize()
{
    if (m_lvl1Tool.retrieve().isFailure()) {
        ATH_MSG_ERROR("Cannot retrieve " << m_lvl1Tool);
        return StatusCode::FAILURE;
    } else {
        ATH_MSG_INFO("Retrieved Lvl1ResultAccessTool");
        return StatusCode::SUCCESS;
    }
}

StatusCode RawFilterAlg::execute() {

    std::bitset<128> triggerBits;
    std::bitset<128> overflowBits;
   // Retrieve the EventInfo:
   const EventInfo* ei = 0;
   ATH_CHECK( evtStore()->retrieve( ei ) );

   const ROIB::RoIBResult* roibresult = 0;
   CHECK (evtStore()->retrieve(roibresult) );

   std::bitset<3> cmxOverflow = m_lvl1Tool->lvl1EMTauJetOverflow(*roibresult);
   if (cmxOverflow.any()) {
       ATH_MSG_DEBUG("TOB overflows in CMX transmission");
   }
   if (roibresult->CheckEMOverflow() || roibresult->CheckJOverflow()) {
       ATH_MSG_DEBUG("Overflows in the EM or JET RoI transmission to the RoIB");
   }

   const std::vector< ROIB::L1TopoResult > l1TopoResults = roibresult->l1TopoResult();
   ATH_MSG_DEBUG( "Number of L1Topo ROI RODs found: " << l1TopoResults.size() );

   if (l1TopoResults.size()==0){
       ATH_MSG_INFO( "Problem: got ROIB but no RODs " );
   }
/**/
   for (auto & r : l1TopoResults){
      //ATH_MSG_VERBOSE( r.dump() );
       auto rdo=r.rdo();
       ATH_MSG_DEBUG( "Found ROI RDO with source ID " << L1Topo::formatHex8(rdo.getSourceID()) );
       auto errors = rdo.getErrors();
       if (! errors.empty()){
           ATH_MSG_INFO( "ROI Converter errors reported: " << errors );
       }
       const std::vector<uint32_t> cDataWords = rdo.getDataWords();
       if ( cDataWords.empty() ) {
           ATH_MSG_INFO( "L1TopoRDO ROI payload is empty" );
       }
       for (auto word : cDataWords){
           ATH_MSG_VERBOSE( "got ROI word: " << L1Topo::formatHex8(word) );
           switch (L1Topo::blockType(word)){
           case L1Topo::BlockTypes::L1TOPO_TOB:
           {
               auto tob = L1Topo::L1TopoTOB(word);
               ATH_MSG_DEBUG( tob );
               // Check for CRC errors on input links which are flagged in header
               int ibin(0);
               for (bool crc: {tob.crc_EM(), tob.crc_Tau(), tob.crc_Muon(), tob.crc_Jet(), tob.crc_Energy()}){
                   if (crc){
                       // ATH_MSG_INFO( "Problem: CRC error " ); // DG-2016-10-31 this happens a bit often, need to check
                       ATH_MSG_VERBOSE( "Problem: CRC error " );
                   }
                   ibin++;
               }
               // collect trigger and overflow bits in bitsets
               for (unsigned int i=0; i<8; ++i){
                   unsigned int index = L1Topo::triggerBitIndexNew(rdo.getSourceID(),tob,i);
                   ATH_MSG_VERBOSE( "filling index " << index
                                    << " with value " << ((tob.trigger_bits()>>i)&1)
                                    << " sourceID " << L1Topo::formatHex8(rdo.getSourceID())
                                    << " tob: idx clk fpga " << tob.index()
                                    << " " << tob.clock()
                                    << " " << tob.fpga()
                                    << " bit " << i);
                   triggerBits[index]  = (tob.trigger_bits()>>i)&1;
                   overflowBits[index] = (tob.overflow_bits()>>i)&1;
               }
               break;
           }
           default:
           {
               ATH_MSG_WARNING( "unexpected TOB type in ROI: " << L1Topo::formatHex8(word) );
               break;
           }
           } // switch(word)
       } // for(word)
   } // for(r)
   
   ATH_MSG_INFO( "trigger  bits from RoI Cnv: " << triggerBits );
   ATH_MSG_INFO( "overflow bits from RoI Cnv: " << overflowBits );
   
   if (overflowBits.any()){
       ATH_MSG_INFO("There is at least one item with overflow: "
                    <<" run "<<ei->event_ID()->run_number()
                    <<" event "<<ei->event_ID()->event_number()
                    <<"\n"
                    <<*(ei->event_ID()));
       setFilterPassed( true );
       m_runs.push_back(ei->event_ID()->run_number());
       m_events.push_back(ei->event_ID()->event_number());
   }

   m_processed_events += 1;

   return StatusCode::SUCCESS;
}

StatusCode RawFilterAlg::finalize() {
    ATH_MSG_INFO("Processed "<<m_processed_events<<" events" );
    ATH_MSG_INFO("List of run / event with l1topo overflows: ("<<m_runs.size()<<" entries)" );
    for(size_t i=0; i<m_runs.size(); ++i)
        ATH_MSG_INFO("DG "
                     <<" run " << m_runs[i]
                     <<" event " << m_events[i]);
        
   return StatusCode::SUCCESS;
}
