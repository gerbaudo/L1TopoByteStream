// Dear emacs, this is -*- c++ -*-
#ifndef L1TOPOBYTESTREAM_RAWFILTERALG_H
#define L1TOPOBYTESTREAM_RAWFILTERALG_H

// Gaudi/Athena include(s):
#include "AthenaBaseComps/AthFilterAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

#include <vector>

namespace HLT {
class ILvl1ResultAccessTool;
}

/// Example filter algorithm
///
/// Algorithm filtering on the event number of the current event.
/// Simply demonstrating how to write only the selected RDO events
/// into an output file.
///
/// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
///
class RawFilterAlg : public AthFilterAlgorithm {

public:
   /// Regular algorithm constructor
   RawFilterAlg( const std::string& name, ISvcLocator* svcLoc );

   /// Function executing the filtering on the current event
   virtual StatusCode initialize() override final;
   virtual StatusCode execute() override final;
   virtual StatusCode finalize() override final;

private:
    ToolHandle<HLT::ILvl1ResultAccessTool> m_lvl1Tool;
    uint64_t m_processed_events;
    std::vector<uint32_t> m_runs;
    std::vector<uint64_t> m_events;
}; // class RawFilterAlg

#endif // L1TOPOBYTESTREAM_RAWFILTERALG_H
