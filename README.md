
```
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup AtlasP1HLT,20.11.0.25,here
git clone ssh://git@gitlab.cern.ch:7999/gerbaudo/L1TopoByteStream.git Trigger/TrigT1/L1Topo/L1TopoByteStream
setupWorkArea.py 
cd WorkArea/cmt/
cmt bro cmt config
cmt bro cmt make
cd - 
cd WorkArea/run/
get_files -jo RAWtoRAW_jobOptions.py

cat << EOF > filelist.txt
root://eosatlas//eos/atlas/atlastier0/rucio/data16_13TeV/express_express/00310015/data16_13TeV.00310015.express_express.merge.RAW/data16_13TeV.00310015.express_express.merge.RAW._lb0185._SFO-ALL._0001.1
EOF

athena RAWtoRAW_jobOptions.py 2>&1 | tee run_athena.txt 
```