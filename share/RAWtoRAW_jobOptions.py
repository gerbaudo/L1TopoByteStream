#
# Example jobO file demonstrating how to run a job that writes selected
# RAW events from an input file to an output file.
#

from AthenaCommon import CfgMgr

from ByteStreamCnvSvc import ReadByteStream
svcMgr.ByteStreamInputSvc.FullFileName = [
    'root://eosatlas//eos/atlas/atlastier0/rucio/data16_13TeV/express_express/00310015/data16_13TeV.00310015.express_express.merge.RAW/data16_13TeV.00310015.express_express.merge.RAW._lb0185._SFO-ALL._0001.1'
    # 'skim_bk_310015.express_express.merge.RAW'
    ]

svcMgr.ByteStreamInputSvc.FullFileName = [l.strip() for l in open('filelist.txt').readlines()]

# include("ByteStreamCnvSvc/BSEventStorageEventSelector_jobOptions.py")
# svcMgr.ByteStreamCnvSvc.InitCnvs += [ "EventInfo", "HLT::HLTResult"]
# svcMgr.ByteStreamCnvSvc.InitCnvs += [ "ROIB::RoIBResult" ]

from AthenaCommon.GlobalFlags import globalflags
globalflags.DetGeo = 'atlas'
globalflags.DataSource = 'data'
from AthenaCommon.DetFlags import DetFlags
DetFlags.detdescr.all_setOff()
DetFlags.readRDOBS.LVL1_setOn()
include('ByteStreamCnvSvcBase/BSAddProvSvc_RDO_jobOptions.py')

# DG-2016-10-31 the outputstreamathenapool doesn't work. The output file is not readable.(?)
# -- # Set up the writing of the output file:
# -- outputFile = 'skim_310015.express_express.merge.RAW'
# -- from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
# -- # rawStream = MSMgr.NewPoolStream( "RAW_TEST", outputFile )
# -- rawStream = MSMgr.NewPoolStream("StreamRDO", outputFile)
# -- rawStream.Stream.TakeItemsFromInput = True
# -- # rawStream.StreamRDO.ForceRead = True # TODO DG check whether this is needed

# Add the filter algorithm to the main algorithm sequence:
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()
topSequence += CfgMgr.RawFilterAlg()

# -- # Set up filtering for the output stream:
# -- rawStream.AddAcceptAlgs( "RawFilterAlg" )

# Process only 100 events for the test:
theApp.EvtMax = -1 # 100

